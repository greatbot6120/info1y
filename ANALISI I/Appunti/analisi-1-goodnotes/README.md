##   Analisi Matematica 1 - notes 

In the `./notes` folder there are three separate files:

*   `analisi-1.goodnotes`:

    >This is the "source" of the notes written with the [Goodnotes](https://www.goodnotes.com/) application. The word "source" is purposely put between quotation marks because the application used for making these notes is definitely **not** open source, anyhow I'm trying to make it "open" for the people who use this app so they can freely modify anything they want.

*   `analisi-1-flattened.pdf`:
    
    >This file is the pdf exported version of `analisi-1.goodnotes`; despite the *not-so-big* filesize, the entire document is a bit laggy when opened with a common pdf reader, due to its *density* I guess. In my experience I usually run it with [Firefox](https://www.mozilla.org/en-US/firefox/new/) or [Okular](https://okular.kde.org/) and it worked just fine.

*   `analisi-1-images.zip`:

    >Goodnotes offer this exporting mode so, why not add it here? 
