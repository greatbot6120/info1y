# Comandi Packet Tracer

## Monitorare stato e parametri del router

### Mostra interfacce di rete

```
Router> show ip interfaces brief
```

### Mostra stato tabella ARP

```
Router> show arp
```

### Mostra stato tabella routing

```
Router> show ip route
```

### Mostra stato tabella routing RIP

```
Router> show ip route rip
```

### Mostra stato protocolli di routing

```
Router> show ip protocols
```

### Mostra stato protocollo di routing RIP

```
Router> show ip rip database
```

### Mostra stato protocollo di routing EIGRP

```
Router> show ip eigrp interfaces
Router> show ip eigrp neighbors
Router> show ip eigrp topology
Router> show ip eigrp traffic
```

### Mostra configurazione attuale

```
Router> enable
Router# show running-config
```

## Configurare parametri generali

### Modifica hostname

```
Router> enable
Router# configure terminal
Router(config)# hostname <nome>
```

### Modifica banner di login

```
Router> enable
Router# configure terminal
Router(config)# banner motd <carattere delimitatore><messaggio><carattere delimitatore>
```

### Modifica password di login

```
Router> enable
Router# configure terminal
Router(config)# enable secret <password>
```

### Disabilitare password di login

```
Router> enable
Router# configure terminal
Router(config)# no enable secret
```

### Salva configurazione attuale su ROM

```
Router> enable
Ruouter# copy running-config startup-config
```

## Configurare parametri di rete

### Configura interfaccia di rete

```
Router> enable
Router# configure terminal
Router(config)# interface <nome interfaccia>
Router(config-if)# ip address <indirizzo ip> <netmask>
Router(config-if)# description <descrizione>
Router(config-if)# clock rate <velocità> (solo per porta seriale)
Router(config-if)# no shutdown
```

### Configura rotta statica

```
Router> enable
Router# configure terminal
Router(config)# ip route <rete> <netmask> <gateway>
```

### Rimuove rotta statica

```
Router> enable
Router# configure terminal
Router(config)# no ip route <rete> <netmask> <gateway>
```

### Configura RIP

```
Router> enable
Router# configure terminal
Router(config)# router rip
Router(config-router)# network <rete>
```

### Configura interfaccia passiva RIP

```
Router> enable
Router# configure terminal
Router(config)# router rip
Router(config-router)# passive-interface <nome interfaccia>
```

### Configura EIGRP

```
Router> enable
Router# configure terminal
Router(config)# router eigrp <numero as>
Router(config-router)# network <rete>
Router(config-router)# metric weights <TOS> <K1> <K2> <K3> <K4> <K5>
```

### Configura DHCP

```
Router> enable
Router# configure terminal
Router(config)# ip dhcp pool <nome pool>
Router(dhcp-config)# network <rete> <netmask>
Router(dhcp-config)# default-router <gateway>
Router(dhcp-config)# dns-server <dns> (opzionale)
```

### Escludere indirizzi DHCP

```
Router> enable
Router# configure terminal
Router(config)# ip dhcp excluded-address <indirizzo iniziale> <indirizzo finale>
```

### Configura NAT

```
Router> enable
Router# configure terminal
Router(config)# interface <interfaccia verso LAN>
Router(config-if)# ip nat inside
Router(config-if)# exit
Router(config)# interface <interfaccia verso WAN>
Router(config-if)# ip nat outside
Router(config-if)# exit
Router(config)# access-list 1 permit <rete> <reciproco_netmask>
Router(config)# ip nat inside source list 1 interface <interfaccia verso WAN> overload
```

### Configura port forwarding

```
Router> enable
Router# configure terminal
Router(config)# ip nat inside source static tcp <IP interno> <porta interna> <IP esterno> <porta esterna>
```
