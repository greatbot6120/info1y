/*
Il sistema informativo di una mensa gestisce i menu e le scelte dei clienti. Ogni giorno il menu contiene n1 scelte per la prima portata e n2 scelte per la
seconda portata (dove n1 e n2 sono comprese fra 2 e 5). Per ogni portata occorre memorizzare il nome della portata (una stringa lunga al più 100 caratteri) e
se contiene glutine. All’inizio della settimana, ciascun cliente deve scegliere per, ogni giorno della settimana, una prima portata e una seconda portata fra quelle
a disposizione. Per ogni giorno e per ogni portata, la scelta viene rappresentata con un numero, compreso fra 1 e ni (dove ni è il numero di scelte disponibili per
quella portata nel menu del giorno).

A.	Definire un tipo di dato portata che contiene tutte le informazioni riguardanti una portata (il nome e se contiene glutine)
B.	Usando il tipo portata appena definito, definire il tipo di dato menu che contiene tutte le informazioni sul menu del giorno
    (numero di scelte per ciascuna portata e le informazioni per ciascuna portata che può essere scelta).

Si considerino le seguenti dichiarazioni di variabili, dove l’array settimana contiene tutti i menu della settimana corrente (dal lunedì al venerdì);
la matrice scelte contiene le portate scelte dal cliente della mensa; ogni riga contiene la scelta della prima portata (prima colonna) e della seconda portata
(seconda colonna) di ciascun giorno.

menu settimana[5];
int scelte[5][2];

C.	Ipotizzando che settimana e scelte siano già stati opportunamente inizializzati, scrivere un frammento di programma che prima di tutto verifichi che le
scelte effettuate siano valide (cioè ogni portata scelta sia compresa fra 1 e il numero di portate previste per quel giorno e quel tipo di portata);
in seguito stampi i nomi delle portate scelte (stampare una riga per ogni giorno della settimana con le due portate separate da una virgola);
infine stampi se il menu contiene glutine.

*/

typedef struct {
    char nome[100];
    int glutine;
} portata;

typedef struct {
    portata primi[5];
    portata secondi[5];
    int n1;
    int n2;
} menu;

int main() {

    menu settimana[5];
    int scelte[5][2];

    /* Inizializzazione settimana[5] e scelte[5][2]... */

    i, corretto = 1;
    for(i=0, i<5 && corretto==1, i++) {
        int sceltaPrimo = scelte[i][0];
        int sceltaSecondo = scelte[i][1];

        if(sceltaPrimo<1 || sceltaPrimo > settimana[i].n1 || sceltaSecondo<1 || sceltaSecondo > settimana[i].n2)
            corretto = 0;
    }

    if(corretto == 0) {
        printf("Il menu non è stato scelto corretamente");
    } else {
        int glutine = 0;
        for(i=0, i<5, i++) {
            int sceltaPrimo = scelte[i][0];
            int sceltaSecondo = scelte[i][1];
            printf("Per il giorno %d è stato scelto: %s, %s\n", i+1, settimana[i].primi[sceltaPrimo].nome, settimana[i].secondi[sceltaSecondo].nome);
            if(glutine == 0)
                if(settimana[i].primi[sceltaPrimo].glutine == 1 || settimana[i].secondi[sceltaSecondo].glutine == 1)
                    glutine = 1;
        }

        if(glutine == 1)
            printf("Il menu della settimana contiene glutuine");
    }

    return 0;
}
