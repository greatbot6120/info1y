/*
Un’azienda registra su file testuali gli ingressi e le uscite dei propri dipendenti. Per ogni dipendente, l’ingresso e l’uscita di una giornata lavorativa sono
organizzati su una riga formattata nel seguente modo:

matricola giorno mese anno ingresso uscita

dove matricola è un codice numerico di 6 cifre che identifica ogni dipendente (un numero a 6 cifre); giorno, mese ed anno rappresentano la data della registrazione
e sono rappresentati in maniera numerica; ingresso ed uscita sono gli orari di ingresso e uscita, rappresentati come numero di minuti trascorsi  dalla  mezzanotte
precedente  (ad esempio, le ore 7:30 corrispondono a 450 minuti, infatti 7 * 60 + 30 = 450).

Ad esempio la seguente riga:

123456 28 2 2017 450 1050

indica che il dipendente con matricola 123456, il giorno 28/2/2017 è entrato alle ore 7:30 (7*60+30 = 450) ed è uscito alle ore 17:30 (17*60+30 = 1050).

Implementare la seguente funzione in C:

int max(int mese, int anno, const char nome[]);

che riceve in ingresso mese, anno e il nome del file su cui sono memorizzati ingressi ed uscite; la funzione deve quindi ritornare la matricola del dipendente che
nel mese e anno ricevuto in ingresso ha passato più tempo al lavoro (il tempo al lavoro viene calcolato come la differenza fra uscita ed entrata analizzando il
file di registro passato come parametro).

Note. Nell’implementare la funzione max, si può ipotizzare che vi siano al massimo 100 dipendenti diversi (e almeno un dipendente). Le righe nel file del registro
ingressi/uscita sono ordinate per numero di matricola. Non si può invece supporre che il file sia ordinato rispetto alle date degli accessi.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    int matricola;
    int oreLavoro;
} dipendente;


int max(int mese, int anno, const char nome[]) {
    FILE *fp;
    fp = fopen(nome, "r");

    int count=-1, max=0;

    dipendente dipendenti[100];

    while(feof(fp) == 0) {
        int matricola, g, m, a, entrata, uscita;

        fscanf(fp, "%d %d %d %d %d %d", &matricola, &g, &m, &a, &entrata, &uscita);

        if(count == -1 || dipendenti[count].matricola != matricola) {
            count++;
            dipendenti[count].matricola = matricola;
            dipendenti[count].oreLavoro = 0;
        }

        if(m == mese && a == anno)
            dipendenti[count].oreLavoro = dipendenti[count].oreLavoro + (uscita - entrata);
    }

    for(int i=0; i<count; i++)
        if(dipendenti[i].oreLavoro > dipendenti[max].oreLavoro)
            max = i;

    return dipendenti[max].matricola;
}

int main() {

    printf("%d\n", max(3, 2017, "registro.txt"));

    return 0;
}
