#include <stdio.h>
int baricentro(int array[], int dim){   /*passa la dimensione dell'array*/
	int i, j, baricentro;
	int somma_sx, somma_dx;
	
	baricentro=-1;             /*baricentro settato a -1, almeno, se non esiste, il programma restituisce -1 come richiesto...*/
	
	for(i=0; i<dim && baricentro<0; i++){           /*...condizione che impongo nel ciclo chiave, almeno, appena trovo un baricentro (che ovviamente � una valore >=0), esco dal ciclo*/
		somma_sx=0;
		somma_dx=0;               /*scorro l'array con un classico ciclo*/
		
		for(j=0; j<=i; j++){
			somma_sx+=array[j];                 /*calcolo la somma degli elementi a sinistra del valore dell'array*/
		}
		
		for(j=i+1; j<dim; j++){                   /*calcolo la somma degli elementi a destra del valore dell'array*/
			somma_dx+=array[j];
		}
		if(somma_dx==somma_sx){     /*se le due somme sono uguali, il valore i � il baricentro ed esiste*/
			baricentro=i;           /*pongo il baricentro pari a i, il baricentro asssume valore >=0, quindi esco dal ciclo chiave*/
		}
	}
	
	return baricentro;
}

