#include <stdio.h>
/*define che preferisci per NC*/
#define BIT 1
void binario(int array[][NC], int nr, int nc, int *c, int *r, int *max){  /*passa le dimensioni dell'array sempre*/    /*elementi da trsmattere==>puntatore*/   /*void...non restituisce nulla*/
	int i, j, cont, len, save_i, save_j;
	
	cont=0;                        
	len=0;
	for(i=0; i<nr; i++){
		for(j=0; j<nc; j++){
			if(array[i][j]==BIT){
				cont++;
			}
			if(array[i][j]==0 || j==nc-1){          /*una sequenza di bit 1 termina o quando trovo uno zero o quando arrivo alla fine della riga(scorrendo l'array per colonne)*/
				if(cont>len){                       /*modfiico i parametri solo se trovo una sequenza pi� lunga della precedente*/  /*� importante mettere cont strettamente maggiore, NON maggiore uguale: questo per restituire la prima sequenza trovata in caso di sequenze con ugaul lunghezza*/  
					len=cont;
					save_j=j-cont+1;             /*ci stiamo muovendo per colonne, quindi attenzione: devi fare j-cont per trasmettere la colonna del primo elemento della sequenza*/
					save_i=i;
				}
				cont=0;        /*ricorda di azzerare il contatore*/
			}
		}
	}
	*r=save_i;         /*ricorda di trasmettere i parametri richiesti*/
	*c=save_j;
	*max=len;
}

