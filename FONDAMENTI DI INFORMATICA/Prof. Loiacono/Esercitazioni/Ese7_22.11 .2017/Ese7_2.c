/*

Scrivere in C una funzione void fun(FILE *f1, FILE *f2) che riceve due puntatori a file (aperti in lettura/scrittura) di tipo binario; f1 contiene una
sequenza di interi e la funzione deve scrivere in f2 (che è vuoto) i numeri pari della sequenza contenuta in f1 in ordine inverso.

*/

#include <stdio.h>

void fun(FILE *f1, FILE *f2);

int main() {

    FILE *out, *in;
    in = fopen("in2.dat", "wb");
    int interi[] = {1,2,3,4,5,6,7,8,9,10,11};
    fwrite(interi, sizeof(int), 11, in);
    fclose(in);

    in = fopen("in2.dat", "rb+");
    out = fopen("out2.dat", "wb+");

    fun(in, out);

    fclose(in);
    fclose(out);

    return 0;

}

void fun(FILE *f1, FILE *f2) {

    int r, x;

    r = fseek(f1, -sizeof(int), SEEK_END); //posiziono il cursore alla fine del fine e torno indietro di uno (della dimensione di un int)

    while(r==0) { //fseek restitusice 0 se ha avuto successo
        if( fread(&x, sizeof(int), 1, f1) != 0 && (x%2 == 0) ) //controlla se il valore letto non è vuoto e se è pari
            fwrite(&x, sizeof(int), 1, f2); //se il valore è pari, lo scrivo nel file f2

        r = fseek(f1, -2*sizeof(int), SEEK_CUR); //posiziono il cursore a -2 posizioni rispetto a dov'è ora
    }
}
